import tkinter as tk
from tkinter import messagebox


class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.create_widgets()
        self.init_frame()
        self.pack()

        self._labelFrame()

    def create_widgets(self):
        self.hi_there = tk.Button(self)
        self.hi_there["text"] = "Hello World\n(click me)"
        self.hi_there["command"] = self.say_hi
        self.hi_there.pack(side="top")

        self.quit = tk.Button(self, text="QUIT", fg="red",
                              command=root.destroy)
        self.quit.pack(side="bottom")

    def _msgBox(self):
        messagebox.showinfo("Say Hello", "Hello World")
        # showinfo
        # showwarning
        # showerror
        # askquestion
        # askokcancel
        # askyesno
        # askretrycancel

    def _labelFrame(self):
        labelframe = tk.LabelFrame(self, text="label on frame")
        labelframe.pack(fill = "both", expand = "yes")
        left = tk.Label(labelframe, text="Inside the LabelFrame")
        left.pack()

    def _spinBox(self):
        spinbox = tk.Spinbox(self, from_=-10, to=10)
        spinbox.pack()

    def _text(self):
        text = tk.Text(self)
        text.insert(tk.INSERT, "XD")
        text.pack()

    def _scrollbar(self):
        self.var = tk.IntVar()
        scrollbar = tk.Scrollbar(self)
        scrollbar.pack( side = tk.RIGHT, fill = tk.Y )
        mylist = tk.Listbox(root, yscrollcommand=scrollbar.set)
        for line in range(100):
            mylist.insert(tk.END, "This is line number " + str(line))

        mylist.pack(side=tk.LEFT, fill=tk.BOTH)
        scrollbar.config(command=mylist.yview)

    def _scale(self):
        self.var = tk.IntVar(self)
        scale = tk.Scale(self, variable = self.var)
        scale.pack()

    def _radio(self):
        self.var = tk.IntVar()
        radio1 = tk.Radiobutton(self, text="Option 1", variable = self.var, value=1, command=self._getVal)
        radio1.pack()
        radio2 = tk.Radiobutton(self, text="Option 2", variable = self.var, value=2, command=self._getVal)
        radio2.pack()
        radio3 = tk.Radiobutton(self, text="Option 3", variable = self.var, value=3, command=self._getVal)
        radio3.pack()

    def _getVal(self):
        print(self.var.get())

    def _mess(self):
        var = tk.StringVar()
        message = tk.Message(self, textvariable=var)
        var.set("Witam, co tam?")
        message.pack()

    def _listBox(self):
        self.xd = tk.Listbox(self)
        self.xd["height"] = 17
        self.xd["selectbackground"] = "red"
        self.xd.insert(1, "Arka")
        self.xd.insert(2, "Gdynia")
        self.xd.insert(3, "*")
        self.xd.insert(2, "Świnia")
        self.xd.pack()

    def _menuButton(self):
        mb = tk.Menubutton(self, text="condiments")
        mb.menu = tk.Menu(mb, tearoff=0)
        mb["menu"] = mb.menu

        mayoVar = tk.IntVar()
        ketchVar = tk.IntVar()

        self.p =  mayoVar

        mb.menu.add_checkbutton(label="mayo",
                                variable=mayoVar)
        mb.menu.add_separator()

        mb.menu.add_checkbutton(label="ketchup",
                                variable=ketchVar)
        mb.pack()

    def _draw(self):
        self.xd = tk.Canvas(self, bg = "blue", height = 250, width = 300)
        self.xd.pack(side = "top")

    def _entry(self):
        self.label = tk.Label(self, text = "User name")
        self.label.pack(side = "top")

        self.varForText = tk.StringVar()
        self.input = tk.Entry(self, textvariable=self.varForText)
        self.input.bind("<Return>", self.key_pressed)
        self.input.pack(side = "bottom")

    def key_pressed(self, event):
        print(self.input.get())


    def _topLevel(self):
        self.f = tk.Toplevel(self)
        self.f.wm_title("XD")

    def _newLabel(self):
        self.var = tk.StringVar()
        self.lab = tk.Label(self, textvariable=self.var)
        self.var.set("WITAM")
        self.lab.pack(side = "bottom")

    def say_hi(self):
        print("hi there, everyone!")

    def init_frame(self):
        self.set_fixed_resolution(600, 400)
        self.set_title("Ticket Machine")

    def set_fixed_resolution(self, width,  height):
        self.master.minsize(width, height)
        self.master.maxsize(width, height)

    def set_title(self, title):
        self.master.title(title)

    def create_button(self, title):
        button = tk.Button()
        button["text"] = title
        button["command"] = self.quit
        button.pack(side = "bottom")

    def create_button_with_image(self, pathtotfile, callback, side_pack):
        img = tk.PhotoImage()
        img["file"] = pathtotfile
        button = tk.Button()
        button["image"] = img
        button["bd"] = 0
        button["command"] = callback
        button.img = img
        button.pack(side = side_pack)

    def load_image(self, path):
        img = tk.PhotoImage()
        img["file"] = path

    def exit(self):
        root.destroy()

root = tk.Tk()
app = Application(master=root)
app.mainloop()
